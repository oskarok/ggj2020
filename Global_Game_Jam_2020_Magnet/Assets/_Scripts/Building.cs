﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Building : MonoBehaviour
{
    public float hp;
    public int maxHP = 100;
    int stateNr = 0;
    SFX sfx;

    public enum state {  HOUSE, WALL, CASTLE };
    public state curState = state.HOUSE;
    public Sprite[] spriteHolder; // example 0-2 wall. 3-5 house. 6-8 castle

    // TakeDmg
    public void ChangeHP(float _dmg)
    {
        if (hp + _dmg <= maxHP)
        {
            hp += _dmg;
        }
        if (hp <= 0)
        {
            Die();
        }
        StateChange();


    }
    void StateChange()
    {
        switch (curState)
        {
            case state.HOUSE:

                if (hp >= 80 && hp <= 90 && stateNr != 3)
                {
                    ChangeSprite(spriteHolder[0]);
                    sfx.upgradeSFX();
                    stateNr = 3;
                }
                else if (hp >= 50 && hp <= 80 && stateNr != 2)
                {
                    ChangeSprite(spriteHolder[1]);
                    sfx.upgradeSFX();
                    stateNr = 2;
                }
                
                else if (hp >= 0 && hp <= 50 && stateNr != 1)
                {
                    ChangeSprite(spriteHolder[2]);
                    sfx.upgradeSFX();
                    stateNr = 1;
                }

                break;
            case state.WALL:
                if (hp >= 80 && hp <= 90 && stateNr != 3)
                {
                    ChangeSprite(spriteHolder[3]);
                    sfx.upgradeSFX();
                    stateNr = 3;
                }
                else if (hp >= 50 && hp <= 80 && stateNr != 2)
                {
                    ChangeSprite(spriteHolder[4]);
                    sfx.upgradeSFX();
                    stateNr = 2;
                }

                else if (hp >= 0 && hp <= 50 && stateNr != 1)
                {
                    ChangeSprite(spriteHolder[5]);
                    sfx.upgradeSFX();
                    stateNr = 1;
                }

                break;
            case state.CASTLE:
                if (hp >= 80 && hp <= 90 && stateNr != 3)
                {
                    ChangeSprite(spriteHolder[6]);
                    sfx.upgradeSFX();
                    stateNr = 3;
                }
                else if (hp >= 50 && hp <= 80 && stateNr != 2)
                {
                    ChangeSprite(spriteHolder[7]);
                    sfx.upgradeSFX();
                    stateNr = 2;
                }

                else if (hp >= 0 && hp <= 50 && stateNr != 1)
                {
                    ChangeSprite(spriteHolder[8]);
                    sfx.upgradeSFX();
                    stateNr = 1;
                }

                break;
            default:
                //ChangeSprite(add sprite here);
                break;
        }
    }
    // Die
    public void Die()
    {
        Destroy(this.gameObject);
        sfx.crumbleSFX();
    }
    public void ChangeSprite(Sprite _sprite)
    {
        GetComponent<SpriteRenderer>().sprite = _sprite;
    }


    // On enter building zone
    private void OnTriggerEnter2D(Collider2D col)
    {
        if (col.tag == "Player")
        {
            col.GetComponent<Stats>().building = this;
            this.transform.GetChild(0).gameObject.SetActive(true);

        }
    }
    // On exit building zone
    private void OnTriggerExit2D(Collider2D col)
    {
        if (col.tag == "Player")
        {
            col.GetComponent<Stats>().building = null;
            this.transform.GetChild(0).gameObject.SetActive(false);

        }
    }
    void Start()
    {
        hp = maxHP;
        hp -= 75;
        sfx = GameObject.FindGameObjectWithTag("MasterObject").GetComponent<SFX>();
        StateChange();

    }
}
