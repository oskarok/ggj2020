﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
    public GameObject enemy;
    Vector3 start;
    void Start()
    {
        StartCoroutine(enemySpawner());
        start = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    IEnumerator enemySpawner()
    {
        Instantiate(enemy, transform.position, Quaternion.identity);
        yield return new WaitForSeconds(1);
        transform.position = start + new Vector3(Random.Range(-10, 10), 0, 0);
        StartCoroutine(enemySpawner());
    }
}
