﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SFX : MonoBehaviour
{
    Stats stats;
    public float speed;
    public AudioClip hammer;    // this lets you drag in an audio file in the inspector
    public AudioClip footsteps;
    public AudioClip wallCrumble;
    public AudioClip upgrade;

    private AudioSource audioFix;
    private AudioSource audioWalk;
    private AudioSource audioCrumble;
    private AudioSource audioUpgrade;

    public void repairSFX()
    {
        
    }


    public void walkSFX()
    {
        if (Input.GetKeyDown(KeyCode.W) || Input.GetKeyDown(KeyCode.A) ||
            Input.GetKeyDown(KeyCode.D) || Input.GetKeyDown(KeyCode.S))
        {

            audioWalk.enabled = true;
            if (!audioWalk.isPlaying)
            {
                audioWalk.Play();
            }

            audioWalk.volume = 0.5f;
            audioWalk.pitch = speed;
            audioWalk.loop = true;

        }
        else if (Input.GetKeyUp(KeyCode.W) || Input.GetKeyUp(KeyCode.A) ||
                Input.GetKeyUp(KeyCode.D) || Input.GetKeyUp(KeyCode.S))
        {
            audioWalk.volume = 0;
            // audioWalk.Stop();
        }
    }

   public void crumbleSFX()
    { 
        audioCrumble.enabled = true;
        audioCrumble.Play();
        
    }

    public void upgradeSFX()
    {
        audioUpgrade.enabled = true;
        audioUpgrade.Play();

    }



    void Start()
    {

        stats = GameObject.FindGameObjectWithTag("Player").GetComponent<Stats>();
        audioFix = gameObject.AddComponent<AudioSource>(); //adds an AudioSource to the game 
        audioWalk = gameObject.AddComponent<AudioSource>(); //object this script is attached to
        audioCrumble = gameObject.AddComponent<AudioSource>();
        audioUpgrade = gameObject.AddComponent<AudioSource>();

        audioFix.clip = hammer;
        audioWalk.clip = footsteps;
        audioCrumble.clip = wallCrumble;
        audioUpgrade.clip = upgrade;


    }

    void Update()
    {
        if (stats.building != null)
        {
            if (Input.GetKeyDown(KeyCode.E))
            {
                audioFix.enabled = true;
                audioFix.Play();
                audioFix.loop = true;
                //audioFix.volume = 0.5f;

            }

            
        }
        if (Input.GetKeyUp(KeyCode.E))
        {
            audioFix.Stop();
        }

    }

  
}


