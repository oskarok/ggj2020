﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Viking : MonoBehaviour
{
    int hp = 10;
    float speed = 2.0f;
    public int fieldOfView = 10;
    float distance = 0.5f;

    float slashCone = 0.06f;
    float projSpeed = 0.2f;

    bool startSlashing = false;

    public Building building;

    BoxCollider2D viking_box;

    Vector3 vel;

    Vector3 distObj, uVector;

    GameObject axe = null;

    [SerializeField]
    GameObject castle;

    GameObject target;

    [SerializeField]
    LayerMask level;

    private void Start()
    {
        target = castle;
        viking_box = GetComponent<BoxCollider2D>();
    }

    void Update()
    {
        // finding a target on the way to the castle
        if (target == castle || target == null)
        {
            target = calculatePriority();
            building = target.GetComponent<Building>();
        }

        // Update path to main objective
        objective(target);


        transform.position += (vel * Time.deltaTime);

        if (building != null && Vector3.Distance(transform.position, building.GetComponent<Transform>().position) < 5)
        {
            building.ChangeHP(-slashCone);
        }

    }
    void objective(GameObject target)
    {
        BoxCollider2D target_box = target.GetComponent<BoxCollider2D>();
        target_box.bounds.Expand(distance);
        if (viking_box.bounds.Intersects(target_box.bounds))
        {
            distObj = new Vector3(Mathf.Cos(target.transform.rotation.z) * (target.transform.position.x - transform.position.x),
                Mathf.Sin(target.transform.rotation.z) * (target.transform.position.y - transform.position.y));
            startSlashing = true;
        }
        else
        {
            distObj = new Vector3(target.transform.position.x - transform.position.x, target.transform.position.y - transform.position.y);
        }
        uVector = distObj / distObj.magnitude;
        vel = uVector * speed;
    }
    GameObject calculatePriority()
    {
        Collider2D[] colliders = Physics2D.OverlapCircleAll(transform.position, fieldOfView, level);

        Debug.Log(colliders.Length);

        // If no other targets are found, go to castle
        if (colliders.Length == 1)
        {
            return castle;
        }

        GameObject priority = null;
        for (int i = 0; i < colliders.Length; i++)
        {
            if (colliders[i].gameObject.tag != "invader")
            {
                if (colliders[i].gameObject.tag == "knight" && priority.tag != "knight")
                {
                    priority = colliders[i].gameObject;
                    break;
                }
                else if (colliders[i].gameObject.tag == "structure" && priority.tag == "wall")
                {
                    priority = colliders[i].gameObject;
                    
                }
                else
                {
                    priority = colliders[i].gameObject;
                }
            }
        }
        return priority;
    }

    void slash(Vector2 unit, float cone)
    {
        /*
        axe.transform.position = new Vector3(transform.position.x + Mathf.Cos(uVector.x - (slashCone/2) + cone) * 2,
            transform.position.y + Mathf.Sin(uVector.y - (slashCone / 2) + cone) * 2);
            */
    }
}